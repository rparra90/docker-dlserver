# Docker DlServer

## Docker run

```bash
nvidia-docker run --restart always -d -p 8000:8000 -p 6006:6006 -v /home/admn/notebooks:/home/jupyter --name dl-server dl-server
```
