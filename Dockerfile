FROM nvidia/cuda:9.0-cudnn7-devel-ubuntu16.04
MAINTAINER Ramón Parra <ramon@rparra.me>

# set up proxy
ENV http_proxy 'http://10.19.21.251:4000'
ENV https_proxy 'http://10.19.21.251:4000'

# update ubuntu
RUN apt-get update
RUN apt-get upgrade -y

# install opencv dependencies
RUN apt-get install -y build-essential cmake pkg-config
RUN apt-get install -y libjpeg8-dev libtiff5-dev libjasper-dev libpng12-dev
RUN apt-get install -y libavcodec-dev libavformat-dev libswscale-dev libv4l-dev
RUN apt-get install -y libxvidcore-dev libx264-dev
RUN apt-get install -y libgtk-3-dev
RUN apt-get install -y libhdf5-serial-dev graphviz
RUN apt-get install -y libopenblas-dev libatlas-base-dev gfortran
RUN apt-get install -y python2.7-dev python3.5-dev
RUN apt-get install -y wget unzip

WORKDIR /tmp

# download the opencv source

RUN wget -O opencv.zip https://github.com/Itseez/opencv/archive/3.4.0.zip
RUN unzip opencv.zip

RUN wget -O opencv_contrib.zip https://github.com/Itseez/opencv_contrib/archive/3.4.0.zip
RUN unzip opencv_contrib.zip

# setup python
RUN apt-get install -y python3-pip
RUN pip3 install --upgrade pip
RUN pip3 install numpy

# compile opencv
RUN mkdir -p /tmp/opencv-3.4.0/build
WORKDIR /tmp/opencv-3.4.0/build
RUN cmake -D CMAKE_BUILD_TYPE=RELEASE \
    -D CMAKE_INSTALL_PREFIX=/usr/local \
    -D WITH_CUDA=OFF \
    -D INSTALL_PYTHON_EXAMPLES=ON \
    -D INSTALL_C_EXAMPLES=OFF \
    -D OPENCV_EXTRA_MODULES_PATH=/tmp/opencv_contrib-3.4.0/modules \
    -D PYTHON_EXECUTABLE=/usr/bin/python3 \
    -D BUILD_EXAMPLES=ON ..
RUN make -j4
RUN make install

RUN mv /usr/local/lib/python3.5/dist-packages/cv2*.so /usr/local/lib/python3.5/dist-packages/cv2.so

# install and configurer jupyter notebook
RUN apt-get install -y npm nodejs-legacy
RUN npm install -g configurable-http-proxy
RUN pip3 install jupyter
RUN pip3 install jupyterhub

RUN useradd -ms /bin/bash jupyter
RUN echo 'jupyter:jupyter' | chpasswd

VOLUME /home/jupyter
WORKDIR /home/jupyter/.jupyterhub

EXPOSE 8000
EXPOSE 6006

# install deep learning libraries
RUN pip3 install scipy matplotlib pillow
RUN pip3 install imutils h5py requests progressbar2
RUN pip3 install scikit-learn scikit-image
RUN pip3 install pandas
RUN pip3 install tensorflow-gpu
RUN pip3 install tensorflow_hub
RUN pip3 install keras

CMD ["jupyterhub"]
